fun main(){
    println("Introduce los numeros de tu carton:")

    val carton = mutableSetOf<Int>()

    for (i in 1.. 10){
        val userNumber = scanner.nextInt()
        carton.add(userNumber)
    }
    println("Numeros cantados:")
    while(carton.isNotEmpty()){

        val number = scanner.nextInt()

        if (carton.contains(number)) {
            carton.remove(number)
        }
        if(carton.isNotEmpty()){
            println("${carton.size} numeros restantes en el carton")
        }
    }
    println("BINGO!!")

}
