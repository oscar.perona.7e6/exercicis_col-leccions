import java.util.*

val scanner = Scanner(System.`in`)

fun main(){
    println("Indroduce el numero de carteles")
    val cartelNumber = scanner.nextInt()
    val carteles = mutableMapOf<Int,String>()
    println("Introduce el kilometro y el texto:")
    for (i in 1..cartelNumber){
        carteles[scanner.nextInt()] = scanner.nextLine()
    }
    println("Cuantas consultas quieres hacer?")
    val consultes = scanner.nextInt()
    println("Introduce las consultas:")
    for (i in 1..consultes){
        val consulta = scanner.nextInt()
        if (consulta in carteles) println(carteles[consulta])
        else println("Cartel no encontrado")
    }
}
