import java.util.*

fun main(){
    println("Introduce las respuestas:")

    val answersList = mutableSetOf<String>()

    do{
        val answer = scanner.next()
        if (answer.uppercase() != "END") {
            if (answersList.contains(answer))println("MEEEC!")
            else answersList.add(answer)
        }
    }while(answer.uppercase() != "END")

}
