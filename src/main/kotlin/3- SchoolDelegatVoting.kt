fun main(){
    println("Introduce los votos:")

    val votos = mutableMapOf<String,Int>()

    do{
        val voto = scanner.next()
        if (voto.uppercase() != "END") {
            if (votos.contains(voto)) {
                votos[voto] = votos[voto]!! + 1

            } else {
                votos[voto] = 1
            }
        }
    }while(voto.uppercase() != "END")

    for (i in votos.keys){
        println("$i : ${votos[i]}")

    }


}
