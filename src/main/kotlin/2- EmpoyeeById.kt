import java.util.*

data class Empleado(
    val name : String,
    val lastName : String,
    val adress : String)

    val empleados = mutableMapOf<String,Empleado>()

fun main(){
    val scanner = Scanner(System.`in`)
    println("Quants empleats vols crear?")
    val number = scanner.nextInt()

    addEmpleados(number)

    println("Introdueix el dni del empleats que vols trobar:")
    do{
        val userDni = scanner.next().uppercase()
        if (userDni != "END"){
            println("${empleados[userDni]?.name} ${empleados[userDni]?.lastName} - $userDni, ${empleados[userDni]?.adress}")
        }
        else println("Saliendo del programa...")

    }while(userDni.uppercase() != "END")

}
fun addEmpleados(n:Int){
    println("Introdueix Dni, nom ,cognom i adreça:")
    for (i in 1..n){
        val dni = readln()
        val nom = readln()
        val cognom = readln()
        val adreca = readln()

        empleados[dni] = Empleado(nom,cognom,adreca)
    }
}
